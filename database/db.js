const MongoClient = require('mongodb').MongoClient;

const PlayersCollectionName = 'players';

class Db {
  /**
   * @param connectionString Mongodb Connection string (example: 'mongodb://localhost:27017')
   */
  constructor (connectionString) {
    this.connectionString = connectionString;
    this.client           = null;
    this.db               = null;
  }

  /**
   * Connect to database
   * @param databaseName Database name
   * @param options Connection options
   * @returns {Promise<void>}
   */
  async connect (databaseName, options = {}) {
    if (!this.client) {
      this.client = await MongoClient.connect(this.connectionString, options);
      this.db     = this.client.db(databaseName);
    }
  }

  /**
   * Disconnect from database
   */
  disconnect () {
    if (this.client) {
      this.db = null;
      this.client.close();
    }
  }

  /**
   * Add players to database
   * @param {object[]} players
   * @returns {Promise<void>}
   */
  async addPlayers (players) {
    return await this.db.collection(PlayersCollectionName).insertMany(players);
  }

  /**
   * Clear players collection
   */
  async clearPlayers () {
    return await this.db.collection(PlayersCollectionName).deleteMany({});
  }

  /**
   * Return mongodb cursor which select all players
   * @returns {Promise<*>}
   */
  async getPlayersCursor () {
    return await this.db.collection(PlayersCollectionName).find({});
  }
}

module.exports = Db;