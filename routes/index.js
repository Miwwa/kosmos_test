const express = require('express');
const router  = express.Router();
const db      = require('../database');
const jobs    = require('../jobs');

const UsersInOneSend = 100;

router.post('/send', async function (req, res) {
  if (typeof req.body.template !== 'string') {
    return res.json({error: true, message: 'template must be a string'});
  }
  const text = req.body.template;

  const cursor = await db.getPlayersCursor();
  let ids  = [];
  // iterate over cursor for not having all players in memory at one time
  while (await cursor.hasNext()) {
    const user = await cursor.next();
    ids.push(user._id);
    //split players to arrays length 100
    if (ids.length === UsersInOneSend) {
      //and make job to send notification to next 100 users
      await jobs.createJob({ids, text});
      ids.length = 0;
    }
  }
  //add last players to sending
  await jobs.createJob({ids, text});

  return res.json({result: true});
});

module.exports = router;