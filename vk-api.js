const MinSendInterval = 300;

let lastUseTime = 0;

/**
 * Send notification to users
 * @param {number[]} ids Users ids
 * @param {string} text Notification text
 */
async function sendNotification (ids, text) {
  if (!Array.isArray(ids) || ids.length === 0 || ids.length > 100) {
    return {code: 3, description: 'Invalid data'};
  }
  if (typeof text !== 'string') {
    return {code: 3, description: 'Invalid data'};
  }

  const now = Date.now();
  if (now - lastUseTime < MinSendInterval) {
    return {code: 1, description: 'Too frequently'};
  }
  lastUseTime = now;

  //fake some job
  const randomTime = Math.floor(Math.random() * 1000);  //0-1 sec
  await wait(randomTime);

  return {
    result: true,
    ids:    ids
  };
}

async function wait (time) {
  return new Promise(resolve => {
    setTimeout(resolve, time);
  });
}

module.exports = {
  sendNotification
};