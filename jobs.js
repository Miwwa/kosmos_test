const config = require('config');
const fs     = require('fs');
const path   = require('path');
const _      = require('lodash');
const Queue  = require('bee-queue');
const vk     = require('./vk-api');

const JobName     = 'sendNotifications';
const JobInterval = config.get('notifications.sendInterval');
const LogsPath    = config.get('notifications.logsPath');
const LogDir      = path.dirname(LogsPath);
if (!fs.existsSync(LogDir)) {
  fs.mkdirSync(LogDir);
}

const sendQueue = new Queue(JobName, {
  redis: config.get('redis'),
  // removeOnSuccess: true,
});

sendQueue.process(async job => {
  const startedAt = Date.now();

  //wait result from api
  let result, err;
  try {
    result = await vk.sendNotification(job.data.ids, job.data.text);
  }
  catch (e) {
    err = e;
  }

  //delay for next call sendNotification
  const finishedAt = Date.now();
  const jobTime    = finishedAt - startedAt;
  if (jobTime < JobInterval)
    await wait(JobInterval - jobTime);

  //if have some error, fail job
  if (err) {
    return Promise.reject(err);
  }
  else if (result.code > 0) {
    return Promise.reject(result);
  }
  //else return ids of players, who received notification
  return result.ids;
});

sendQueue.on('succeeded', function (job, sentIds) {
  const notSentIds = _.xor(job.data.ids, sentIds);
  if (notSentIds > 0) {
    console.log('not sent to ', notSentIds);
  }
  //write log
  fs.appendFile(LogsPath, sentIds.join('\n') + '\n', 'utf-8', err => {
    if (err) {
      console.error('Log write error ', err.message);
    }
  });
});

sendQueue.on('failed', async function (job, err) {
  console.error('failed', job.id, err);
  switch (err.code) {
    case 1:     //Too frequently
      // retry, add same job to queue end
      await createJob(job.data);
      break;
    case 2:     // Server fatal error
    case 3:     // Invalid data
    default:
      //unexpected error, just close application
      process.exit();
      break;
  }
});

async function wait (time) {
  return new Promise(resolve => {
    setTimeout(resolve, time);
  });
}

/**
 * Add new job to queue
 * @param {object} data Job data
 * @returns {Promise<void>}
 */
async function createJob (data) {
  sendQueue.createJob(data).save();
}

module.exports = {
  createJob
};